[![Build status](https://ci.appveyor.com/api/projects/status/5vukaxyby39ujrod?svg=true)](https://ci.appveyor.com/project/tamachan/binaryeditorbz)

## [ダウンロードへ移動→](https://bitbucket.org/tamachan/binaryeditorbz/downloads/)

## [ヘルプへ移動→](http://devil-tamachan.github.io/BZDoc/)

[c.mosさん](http://www.vcraft.jp/)作、[Binary Editor Bz](http://www.vcraft.jp/soft/bz.html)の改造版です 
c.mosさんのご好意でソースが頂けました。それをtamachanが改造しました。 

![](http://devil-tamachan.github.io/BZDoc/image/bzscratch6.png)

今後のアップデート予定：

 * 1.9.X (近日) - 機能追加、バグ修正、最適化
 * 2.0 (未定) - 安定したら・・・
 * X.X (未定) - RAWディスクサポートとか機能追加とか
 * Fillの高速/省メモリ化
 * キャッシュをHDDへ退避させ、メモリ空間以上の編集を可能にする機能
 * ２以上の分割表示
 * バグ修正
 * BMP表示の強化
 * あとチマチマとした機能
 * 英語版リソースの補修

優先度低：

 * HTML5へポート


バグレポート、要望は [Issues](https://bitbucket.org/tamachan/binaryeditorbz/issues?status=new&status=open) へどうぞ 

↓ Bz1.9.8

![](https://raw.githubusercontent.com/devil-tamachan/binaryeditorbz/master/HP/bzscr20151222.png)

 MacOSX版のスクリーンショット20130606

![](https://raw.githubusercontent.com/devil-tamachan/binaryeditorbz/master/HP/macalpha20130606.png)